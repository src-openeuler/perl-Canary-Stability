Name:           perl-Canary-Stability
Version:        2013
Release:        5
Summary:        Canary to check perl compatibility for schmorp's modules
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Canary-Stability
Source0:        https://cpan.metacpan.org/authors/id/M/ML/MLEHMANN/Canary-Stability-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  make perl-generators perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
Requires:       perl(ExtUtils::MakeMaker)

%description
This module is used by Schmorp's modules during configuration stage to test
the installed perl for compatibility with this modules.

%prep
%setup -q -n Canary-Stability-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%{make_build}

%install
%{make_install}
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%{perl_vendorlib}/*
%license COPYING
%doc Changes README
%{_mandir}/man3/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 2013-5
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Mar  2 2020 openEuler Buildteam <buildteam@openeuler.org> - 2013-4
- Package init
